--[[
-Code courtesy of penguin0616. Most (Almost all, actually) belongs to Insight. https://steamcommunity.com/sharedfiles/filedetails/?id=2189004162
Adapted for Island Adventures - Shipwrecked. 
]]

-- twisterspawner.lua [Worldly]
local TWISTER_TIMERNAME = "twister_timetoattack"
local function GetTwisterData(self)
	if TWISTER_TIMERNAME == false then
		return {}
	end

	if not self.inst.updatecomponents[self] then
		return {}
	end

	local save_data = self:OnSave()

	local time_to_attack
	if CurrentRelease.GreaterOrEqualTo("R15_QOL_WORLDSETTINGS") then
		time_to_attack = TheWorld.components.worldsettingstimer:GetTimeLeft(TWISTER_TIMERNAME)
	else
		time_to_attack = save_data.timetoattack
	end
	local target, upvalue_exists = Insight.env.util.getupvalue(self.OnUpdate, "_targetplayer")
	local target_error_string = nil

	if target then
		target = {
			name = target.name,
			userid = target.userid,
			prefab = target.prefab,
		}
	else
		if upvalue_exists == false then
			target_error_string = "???"
		end
	end

	return {
		time_to_attack = time_to_attack,
		target = target,
		warning = save_data.warning,
		target_error_string = target_error_string
	}
end

local function ProcessInformation(context, time_to_attack, target, target_error_string)
	local time_string = context.time:SimpleProcess(time_to_attack)
	local client_table = target and TheNet:GetClientTableForUser(target.userid)

	if not client_table then
		if target_error_string then
			return string.format(
				STRINGS.INSIGHT.TWISTERSPAWNER.INCOMING_TWISTER_TARGETED,
				"#cc4444",
				target_error_string,
				time_string
			)
		end

		return time_string
	else
		local target_string = string.format("%s - %s", target.name, target.prefab)
		return string.format(
			STRINGS.INSIGHT.TWISTERSPAWNER.INCOMING_TWISTER_TARGETED,
			Insight.env.Color.ToHex(
				client_table.colour
			),
			target_string,
			time_string
		)
	end
end

local function Describe(self, context)
	local description = nil
	local data = {}

	if self == nil and context.twister_data then
		data = context.twister_data
	elseif self and context.twister_data == nil then
		data = GetTwisterData(self)
	else
		error(string.format("twisterspawner.Describe improperly called with self=%s & twister_data=%s", tostring(self), tostring(context.twister_data)))
	end

	if data.time_to_attack then
		description = ProcessInformation(context, data.time_to_attack, data.target, data.target_error_string)
	end

	return {
		priority = 10,
		description = description,
		icon = {
			atlas = "images/Twister.xml",
			tex = "Twister.tex",
		},
		worldly = true,
		time_to_attack = data.time_to_attack,
		target_userid = data.target and data.target.userid or nil,
		warning = data.warning,
	}
end

local function StatusAnnoucementsDescribe(special_data, context)
	if not special_data.time_to_attack then
		return
	end

	local description = nil
	local target = special_data.target_userid and TheNet:GetClientTableForUser(special_data.target_userid)

	if target then
		description = Insight.env.ProcessRichTextPlainly(string.format(
			STRINGS.INSIGHT.TWISTERSPAWNER.ANNOUNCE_TWISTER_TARGET,
			target.name,
			target.prefab,
			context.time:TryStatusAnnouncementsTime(special_data.time_to_attack)
		))
	else
		description = Insight.env.ProcessRichTextPlainly(string.format(
			STRINGS.INSIGHT.TWISTERSPAWNER.TWISTER_ATTACK,
			context.time:TryStatusAnnouncementsTime(special_data.time_to_attack)
		))
	end

	return {
		description = description,
		append = true
	}
end

local function DangerAnnouncementDescribe(special_data, context)
	-- Funny enough, very similar to logic for status announcements and normal descriptor.
	-- Gets repetitive.
	if not special_data.time_to_attack then
		return
	end

	local description
	local client_table = special_data.target_userid and TheNet:GetClientTableForUser(special_data.target_userid)
	local time_string = context.time:SimpleProcess(special_data.time_to_attack, "realtime")

	if not client_table then
		description = string.format(STRINGS.INSIGHT.TWISTERSPAWNER.TWISTER_ATTACK, time_string)
	else
		description = string.format(
			STRINGS.INSIGHT.TWISTERSPAWNER.ANNOUNCE_TWISTER_TARGET,
			client_table.name,
			client_table.prefab,
			time_string
		)
	end

	return description, "boss"
end

return {
	Describe = Describe,
	GetTwisterData = GetTwisterData,
	StatusAnnoucementsDescribe = StatusAnnoucementsDescribe,
	DangerAnnouncementDescribe = DangerAnnouncementDescribe,
}