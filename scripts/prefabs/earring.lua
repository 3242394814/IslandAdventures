local assets =
{
	Asset("ANIM", "anim/earring.zip"),
}

local function shine(inst)
    inst.task = nil

    if inst.components.floater:IsFloating() then
        inst.AnimState:PlayAnimation("sparkle_water")
        inst.AnimState:PushAnimation("idle_water")
    else
        inst.AnimState:PlayAnimation("sparkle")
        inst.AnimState:PushAnimation("idle")
    end

    if inst.entity:IsAwake() then
        inst.task = inst:DoTaskInTime(4+math.random() * 5, shine)
    end
end

local function fn()

	local inst = CreateEntity()
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	inst.entity:AddPhysics()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("earring")
    inst.AnimState:SetBuild("earring")
    inst.AnimState:PlayAnimation("idle")

    inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )

    MakeInventoryPhysics(inst)
    
    inst:AddTag("trinket")

    MakeInventoryFloatable(inst)
    inst.components.floater:UpdateAnimations("idle_water", "idle")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")
    inst:AddComponent("stackable")
    inst:AddComponent("inventoryitem")

    inst:AddComponent("tradable")
    inst.components.tradable.goldvalue = 3
    inst.components.tradable.dubloonvalue = 12

    inst:AddComponent("appeasement")
    inst.components.appeasement.appeasementvalue = TUNING.APPEASEMENT_HUGE

    shine(inst)

    MakeHauntableLaunch(inst)

    return inst
end

return Prefab("earring", fn, assets)
