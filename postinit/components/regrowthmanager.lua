local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

local BASE_RADIUS = 20
local EXCLUDE_RADIUS = 3

local REGROWBLOCKER_ONEOF_TAGS = { "structure", "wall", "regrowth_blocker" }

IAENV.AddComponentPostInit("regrowthmanager", function(self)
    local _TestForRegrow = UpvalueHacker.GetUpvalue(self.LongUpdate, "DoRegrowth", "TestForRegrow")
    local function TestForRegrow(x, y, z, orig_tile, ...)
        if IsOceanTile(orig_tile) then
            if TheWorld.Map:GetTileAtPoint(x, y, z) ~= orig_tile then
                -- keep things in their biome (more or less)
                return false
            end
        
            local ents = TheSim:FindEntities(x,y,z, EXCLUDE_RADIUS)
            if #ents > 0 then
                -- Too dense
                return false
            end
        
            local ents = TheSim:FindEntities(x,y,z, BASE_RADIUS, nil, nil, REGROWBLOCKER_ONEOF_TAGS)
            if #ents > 0 then
                -- No regrowth around players and their bases
                return false
            end
            return true
        else
            return _TestForRegrow(x, y, z, orig_tile, ...)
        end
    end
    UpvalueHacker.SetUpvalue(self.LongUpdate, TestForRegrow, "DoRegrowth", "TestForRegrow")

    local _worldstate = TheWorld.state

    self:SetRegrowthForType("sweet_potato_planted", TUNING.SWEET_POTATO_REGROWTH_TIME, "sweet_potato_planted", function()
        return not (_worldstate.isnight or _worldstate.iswinter or _worldstate.snowlevel > 0) and TUNING.SWEET_POTATO_REGROWTH_TIME_MULT or 0
    end)

    self:SetRegrowthForType("crabhole", TUNING.RABBITHOLE_REGROWTH_TIME, "crabhole", function()
        return (_worldstate.issummer and TUNING.RABBITHOLE_REGROWTH_TIME_SUMMER_MULT or TUNING.RABBITHOLE_REGROWTH_TIME_MULT > 0) and 1 or 0
    end)

    self:SetRegrowthForType("coral_brain_rock", TUNING.CORAL_BRAIN_REGROW_TIME, "coral_brain_rock", function()
        return TUNING.CORAL_BRAIN_REGROW_TIME_MULT
    end)

    self:SetRegrowthForType("shipwreck", TUNING.SHIPWRECK_REGROW_TIME, "shipwreck", function()
        return _worldstate.iswinter and (_worldstate.isnight and 2 or 1) or 0
    end)

    self:SetRegrowthForType("waterygrave", TUNING.WATERYGRAVE_REGROW_TIME, "waterygrave", function()
        return _worldstate.iswinter and (_worldstate.isnight and 2 or 1) or 0
    end)

    self:SetRegrowthForType("seashell_beached", TUNING.SEASHELL_BEACHED_REGROW_TIME, "seashell_beached", function()
        return TUNING.SEASHELL_BEACHED_REGROW_TIME_MULT
    end)

    -- TODO Add configs for them
end)
